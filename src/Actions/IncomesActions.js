import firebase from "../firebase";

export const getIncomes = () => async (dispatch, getState) => {
  dispatch({ type: "FETCH_INCOMES" });

  const incomesRef = firebase
    .database()
    .ref("incomes")
    .orderByChild("createdAt");
  incomesRef.on("value", (snapshot) => {
    dispatch({ type: "INCOMES_RECEIVE", value: snapshot.val() });
  });
};

export const deleteIncome = (incomeId) => async (dispatch, getState) => {
  dispatch({ type: "FETCH_INCOME_DELETE" });
  
  const incomeDeleteRef = firebase.database().ref("incomes")

  incomeDeleteRef
    .child(incomeId)
    .remove()
    .then((data) => {
      dispatch({ type: "INCOME_DELETE_SUCCESS" });
    }).catch((error) => {
      dispatch({ type: "INCOME_DELETE_ERROR", value: 'Não foi possivel excluir essa renda' });
    });
};

export const createIncome = (object, hooks = {}) => async (dispatch) => {
  dispatch({ type: "FETCH_INCOME_CREATE" });

  object.value = object.value.replace(/\./g, "").replace(",", ".");
  firebase.database().ref("incomes").push(object).then(data => {
    dispatch({ type: "INCOME_CREATE_SUCCESS" });
    hooks.onComplete()
  }).catch(error => {
    dispatch({ type: "INCOME_CREATE_ERROR", value: 'Não foi possivel criar essa renda' });
    hooks.onComplete();
  });
}