import React from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom";

import PrivateRoute from "./PrivateRoute";

import Income from "./Views/Income/Income";
import Login from "./Views/Account/Login";
import Signup from "./Views/Account/SignUp";
import NotFound from "./Views/NotFound/NotFound";

export const URL_HOME = `/`
export const URL_INVESTMENT = `/`
export const URL_LOGIN = `/login`;
export const URL_SIGNUP = `/criar-usuario`;

const RootRoute = () => (
  <BrowserRouter>
    <Switch>
      <PrivateRoute path={URL_INVESTMENT} exact={true} component={Income} />
      <Route path={URL_LOGIN} exact={true} component={Login} />
      <Route path={URL_SIGNUP} exact={true} component={Signup} />
      <Route path="*" component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default RootRoute;