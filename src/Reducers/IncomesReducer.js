const initialState = {
  isLoading: true,
  isLoadingCreate: null,
  incomesByIds: null,
};

export const incomesReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_INCOMES":
      return {
        ...state,
        isLoading: true,
      };
    case "INCOMES_RECEIVE":
      const incomes = action.value
        ? Object.entries(action.value).reduce((all, curr) => {
            curr[1].id = curr[0];
            return { ...all, [curr[0]]: curr[1] };
          }, {})
        : null;
      return {
        ...state,
        isLoading: false,
        incomesByIds: incomes,
      };
    case "FETCH_INCOME_CREATE":
      return {
        ...state,
        isLoadingCreate: true,
      };
    case "INCOME_CREATE_SUCCESS":
      return {
        ...state,
        isLoadingCreate: false,
      };
    case "INCOME_CREATE_ERROR":
      return {
        ...state,
        isLoading: false,
        error: action.value,
      };
    case "FETCH_INCOME_DELETE":
      return {
        ...state,
        isLoading: true,
      };
    case "INCOME_DELETE_SUCCESS":
      return {
        ...state,
        isLoading: false,
        error: null,
      };
    case "INCOME_DELETE_ERROR":
      return {
        ...state,
        isLoading: false,
        error: action.value,
      };

    default:
      return state;
  }
};
