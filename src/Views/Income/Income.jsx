import React from 'react';
import { withRouter } from 'react-router-dom';

import HeaderComponent from '../Header/HeaderComponent';
import IncomeTable from './IncomeTable';
import IncomeForm from './IncomeForm';

import './Styles/Income.scss';

class Income extends React.Component {

  constructor() {
    super()
    this.state = {
      showModal: false
    }
  }
  
  toggleModal = (showModal) => {
    this.setState({ showModal })
  }
  
  render() {
    return (
      <div>
        <HeaderComponent />
        <section className="section-income">
          <header className="income__header">
            <h1 className="income__header-title">Meus investimentos</h1>
            <button
              className="btn btn--green"
              onClick={() => this.toggleModal(true)}
            >
              Adicionar investimento
              <i className="demo-icon icon-plus" />
            </button>
          </header>

          <section className="income__form">
            <IncomeForm
              showModal={this.state.showModal}
              closeModal={this.toggleModal}
            />
          </section>

          <IncomeTable />
        </section>
      </div>
    );
  }
}

export default withRouter(Income);
