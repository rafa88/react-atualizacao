import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { createIncome } from '../../Actions/IncomesActions';
import { InputMaskMoney, } from "../Helpers/Mask";
import { formDataToObject } from '../Helpers/dataToObject';

import './Styles/IncomeForm.scss';

class IncomeForm extends React.Component {
  state = {
    values: {
      type: "",
      value: "",
      date: "",
    },
    displayErrors: null
  };

  closeModal(e) {
    e.preventDefault();
    this.props.closeModal(false);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    if (!e.target.checkValidity()) {
      this.setState({
        invalid: true,
        displayErrors: true,
      });
      return;
    }
    this.props.createIncome(formDataToObject(data), {
      onComplete: () => {
        this.setState({ values: {type: "", value: "", date: ""} });
      }
    });
  };

  onChange = (field, value) => {
    const { values } = this.state;
    values[field] = value;
    this.setState({ values });
  };

  render() {
    const { showModal, isLoadingCreate } = this.props;
    const { displayErrors } = this.state;

    return (
      <section
        className={`modal-form ${showModal ? "modal-form__active" : null}`}
      >
        <form
          className={`form ${displayErrors ? "form__errors" : ""}`}
          onSubmit={this.handleSubmit}
        >
          <h3>Cadastro do meu investimento</h3>
          <div className="input-group">
            <select
              className="input-element input-element--select"
              name="type"
              required
              value={this.state.values.type}
              onChange={(ev) => this.onChange("type", ev.target.value)}
            >
              <option value="">Tipo</option>
              <option value="RENDA_VARIAVEL">Renda Variavel</option>
              <option value="RENDA_FIXA">Renda Fixa</option>
            </select>
          </div>
          <div className="input-group">
            <input
              type="text"
              name="value"
              className="input-element"
              placeholder="valor"
              value={this.state.values.value}
              required
              autoComplete="off"
              onChange={(ev) => {
                this.onChange("value", InputMaskMoney(ev.target.value));
              }}
            />
          </div>
          <div className="input-group">
            <input
              type="date"
              name="date"
              className="input-element"
              placeholder="Data de compra"
              value={this.state.values.date}
              maxLength={10}
              pattern="\d{2}\/\d{2}/\d{4}"
              required
              autoComplete="off"
              onChange={(ev) => this.onChange("date", ev.target.value)}
            />
          </div>
          <div className="input-group__button">
            <button className="btn btn--stroke" onClick={(e) => { this.closeModal(e); }} disabled={isLoadingCreate}>Cancelar</button>
            <button type="submit" className="btn btn--green" disabled={isLoadingCreate}>
              Adicionar {isLoadingCreate && <i className="demo-icon icon-spin6 animate-spin"></i>}
            </button>
          </div>
        </form>
      </section>
    );
  }
}

export default withRouter(
  connect(
    (state) => {
      return {
        isLoadingCreate: state.incomes.isLoadingCreate
      };
    },
    (dispatch) => {
      return {
        createIncome: (object, callback) => {
          dispatch(createIncome(object, callback));
        }
      };
    }
  )(IncomeForm)
);
