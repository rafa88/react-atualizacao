import React from "react";
import { withRouter } from "react-router-dom";

import HeaderComponent from "../Header/HeaderComponent";

class NotFound extends React.Component {
  render() {
    return (
      <div>
        <HeaderComponent />
        <h1>Pagina não encontrada</h1>
      </div>
    );
  }
}

export default withRouter(NotFound);
