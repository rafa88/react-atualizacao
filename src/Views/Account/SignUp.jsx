import React from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";

import { formDataToObject } from "../Helpers/dataToObject";
import { createUser } from "../../Actions/AuthActions";
import { URL_LOGIN, URL_HOME } from "../../RootRoute";
import { mapError } from "../../Enum";

import HeaderComponent from "../Header/HeaderComponent";
import ToastComponent from "..//Toast/ToastComponent";

import "./Styles/Account.scss";

class SignUp extends React.Component {
  state = {
    values: {
      email: null,
      password: null,
    },
    displayErrors: null
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    if (!e.target.checkValidity()) {
      this.setState({
        invalid: true,
        displayErrors: true,
      });
      return;
    }

    this.props.createUser(formDataToObject(data), () => this.props.history.push(URL_HOME));
  };

  onChange = (field, event) => {
    const { values } = this.state;
    values[field] = event.target.value;
    this.setState({ values });
  };

  render() {
    const { errorMessage, isLoadingAuth } = this.props;

    return (
      <div>
        <HeaderComponent />
        <section className="section-login">
          <form className="form" onSubmit={this.handleSubmit}>
            <h1 className="login__title">Criar sua usuário</h1>
            <div className="input-group">
              <input
                type="email"
                name="email"
                className="input-element"
                placeholder="email"
                autoComplete="off"
                required
                onChange={(ev) => this.onChange("email", ev)}
              />
            </div>
            <div className="input-group">
              <input
                type="password"
                name="password"
                className="input-element"
                placeholder="senha"
                autoComplete="off"
                required
                onChange={(ev) => this.onChange("password", ev)}
              />
            </div>
            <div className="input-group__button">
              <Link to={URL_LOGIN} className="btn btn--stroke">
                Fazer login
              </Link>
              <button
                type="submit"
                className="btn btn--green"
                disabled={isLoadingAuth}
              >
                Adicionar
                {isLoadingAuth && (
                  <i className="demo-icon icon-spin6 animate-spin"></i>
                )}
              </button>
            </div>
          </form>
        </section>
        {errorMessage && (
          <ToastComponent
            message={mapError[errorMessage] || errorMessage}
            type="error"
          />
        )}
      </div>
    );
  }
}

export default withRouter(
  connect(
    (state) => {
      return {
        isLoadingAuth: state.auth.isLoading,
        errorMessage: state.auth.error ? state.auth.error.signUp : null,
      };
    },
    (dispatch) => {
      return {
        createUser: (values, callback) => dispatch(createUser(values, callback)),
      };
    }
  )(SignUp)
);
