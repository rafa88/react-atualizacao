import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PieChart from './PieChart';

class PieChartComponent extends Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();

    this.createChart = this.createChart.bind(this);
  }

  getInitialState() {
    return {
      chart: null,
    };
  }

  componentDidMount() {
    this.createChart();
    window.addEventListener('resize', this.createChart);
  }

  componentWillReceiveProps(nextProps) {
    const { chart } = this.state;
    const { data, cumulative } = this.props;
    if (chart && (JSON.stringify(nextProps.data) !== JSON.stringify(data) || cumulative !== nextProps.cumulative)) {
      chart.update(nextProps);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.createChart);
  }

  createChart() {
    const el = ReactDOM.findDOMNode(this.refs.chart);
    const { chart } = this.state
    if (chart) {
      chart.destroy()
    }

    const margin = {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
    };
    const { width } = this.props
    const elWidth = width || el.offsetWidth

    const props = {
      margin,
      width: elWidth,
      height: 350,
    };

    this.setState({
      chart: new PieChart(el, props),
    }, function callback() {
      this.state.chart.create();
      this.state.chart.update(this.props);
    });
  }

  render() {
    return (
      <div
        ref='chart'
      />
    );
  }
}

export default PieChartComponent