export const mapIncome = Object.freeze({
	RENDA_VARIAVEL: "Renda Variavel",
	RENDA_FIXA: "Renda Fixa"
})

export const mapError = Object.freeze({
  "auth/email-already-in-use": "E-mail já cadastrado",
  "auth/user-not-found": "Usuário não encontrado",
  "auth/wrong-password": "Senha inválida"
})