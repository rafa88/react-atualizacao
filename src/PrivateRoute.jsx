import React from "react";
import { Route, Redirect } from "react-router-dom";
import firebase from "./firebase";
import { URL_LOGIN } from "./RootRoute";

class PrivateRoute extends React.Component {
  constructor() {
    super();
    this.state = { isAuthenticated: null };
  }
  
  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      const isAuthenticated =
        user || sessionStorage.getItem("reactInvestimentoToken") ? true : false;
      this.setState({ isAuthenticated });
    });
  }

  render() {
    const { component: Component, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={(props) =>
          this.state.isAuthenticated || sessionStorage.getItem('reactInvestimentoToken') ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{ pathname: URL_LOGIN, state: { from: props.location } }}
            />
          )
        }
      />
    );
  }
}

export default PrivateRoute;
