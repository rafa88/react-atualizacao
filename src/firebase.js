import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAioziagfpBVwOPlgi1QtfiyxfHNNTgdNk",
  authDomain: "react-investimento.firebaseapp.com",
  databaseURL: "https://react-investimento.firebaseio.com",
  projectId: "react-investimento",
  storageBucket: "react-investimento.appspot.com",
  messagingSenderId: "1017396634165",
  appId: "1:1017396634165:web:2e3d532fe03072e741077c",
  measurementId: "G-811T8GND4R",
};

firebase.initializeApp(firebaseConfig);
export default firebase